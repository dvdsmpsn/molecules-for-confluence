/*    */ package me.davidsimpson.confluence.plugins.molecules;
/*    */ 
/*    */ import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
/*    */ import com.atlassian.confluence.util.velocity.VelocityUtils;
/*    */ import com.atlassian.renderer.RenderContext;
/*    */ import com.atlassian.renderer.v2.RenderMode;
/*    */ import com.atlassian.renderer.v2.macro.BaseMacro;
/*    */ import com.atlassian.renderer.v2.macro.MacroException;
/*    */ import java.util.Map;
/*    */ 
/*    */ public class MoleculesMacro extends BaseMacro
/*    */ {
/*    */   public boolean isInline()
/*    */   {
/* 29 */     return false;
/*    */   }
/*    */ 
/*    */   public boolean hasBody()
/*    */   {
/* 34 */     return false;
/*    */   }
/*    */ 
/*    */   public RenderMode getBodyRenderMode()
/*    */   {
/* 39 */     return RenderMode.NO_RENDER;
/*    */   }
/*    */ 
/*    */   public String execute(Map params, String body, RenderContext renderContext)
/*    */     throws MacroException
/*    */   {
/* 51 */     String macroBodyTemplate = "templates/molecules-macro.vm";
/*    */ 
/* 54 */     Map velocityContext = MacroUtils.defaultVelocityContext();
/*    */ 
/* 56 */     return VelocityUtils.getRenderedTemplate(macroBodyTemplate, velocityContext);
/*    */   }
/*    */ }

/* Location:           /Users/davidsimpson/projects/davidsimpson.me/confluence/molecules-for-confluence/src/main/java/
 * Qualified Name:     me.davidsimpson.confluence.plugins.molecules.MoleculesMacro
 * JD-Core Version:    0.6.2
 */