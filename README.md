# Molecules for Confluence

- A project started and released to plugins.atlassian.com sometime back in 2010
- The source code was lost back when studio.plugins.atlassian.com was decommissioned
- This source project has been reconstructed from the published jar file
- This is based off [CanvasMol](http://alteredqualia.com/canvasmol/) by [@alteredq](http://twitter.com/alteredq)

## Done:

- Migrate source to Bitbucket

## TODO:

- check that this thing really compiles
- update the macro for Confluence 4+ (really) or maybe just 5+
- add some marketing assets - logo, macro browse icon etc
- release it on Atlassian's Marketplace